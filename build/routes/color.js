"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var color_1 = __importDefault(require("../controller/color/color"));
var getcolor_1 = __importDefault(require("../controller/color/getcolor"));
var allcolors_1 = __importDefault(require("../controller/color/allcolors"));
var Color = /** @class */ (function () {
    function Color() {
    }
    Color.prototype.routes = function (app) {
        app.route('/color')
            .post(color_1.default);
        app.route('/getcolor')
            .post(getcolor_1.default);
        app.route('/colors')
            .get(allcolors_1.default);
    };
    return Color;
}());
exports.Color = Color;
// app.route(upload.single("prod_image"),newProduct)
//# sourceMappingURL=color.js.map