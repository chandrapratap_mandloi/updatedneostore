"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var category_1 = __importDefault(require("../controller/product/category"));
var multer_1 = __importDefault(require("../config/multer"));
var product_1 = __importDefault(require("../controller/product/product"));
var verifytoken_1 = __importDefault(require("../config/verifytoken"));
var checkout_1 = __importDefault(require("../controller/product/checkout"));
var findproduct_1 = __importDefault(require("../controller/product/findproduct"));
var findproducts_1 = __importDefault(require("../controller/product/findproducts"));
var cart_1 = __importDefault(require("../controller/product/cart"));
var showcart_1 = __importDefault(require("../controller/product/showcart"));
var toprated_1 = __importDefault(require("../controller/product/toprated"));
var allcategory_1 = __importDefault(require("../controller/product/allcategory"));
var updaterating_1 = __importDefault(require("../controller/product/updaterating"));
var getorder_1 = __importDefault(require("../controller/product/getorder"));
var deletecart_1 = __importDefault(require("../controller/product/deletecart"));
var suggestion_1 = __importDefault(require("../controller/product/suggestion"));
var invoice_1 = __importDefault(require("../controller/product/invoice"));
var Product = /** @class */ (function () {
    function Product() {
    }
    Product.prototype.routes = function (app) {
        app.route('/category')
            .post(multer_1.default.single('category_image'), category_1.default);
        app.route('/product')
            .post(multer_1.default.array('product_image', 12), product_1.default);
        app.route('/allcategory')
            .get(allcategory_1.default);
        app.route('/findproduct')
            .post(findproduct_1.default);
        app.route('/findproducts')
            .get(findproducts_1.default);
        app.route('/cart')
            .post(verifytoken_1.default, cart_1.default);
        app.route('/showcart')
            .get(verifytoken_1.default, showcart_1.default);
        app.route('/rating')
            .post(verifytoken_1.default, updaterating_1.default);
        // app.route('/checkout')
        // .get(verifyToken,checkout)
        app.route('/checkout')
            .get(checkout_1.default);
        app.route('/showproducts')
            .get(toprated_1.default);
        app.route('/order')
            .get(verifytoken_1.default, getorder_1.default);
        // app.route('/deletecart/:cart_Id')
        // .delete(verifyToken, deletecart)
        app.route('/deletecart/:cart_id')
            .delete(verifytoken_1.default, deletecart_1.default);
        app.route('/suggestion')
            .get(suggestion_1.default);
        // app.route('/invoice/:_id')
        // .get(invoice)
        app.route('/invoice')
            .post(verifytoken_1.default, invoice_1.default);
    };
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map