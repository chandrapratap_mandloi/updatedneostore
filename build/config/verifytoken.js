"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var verifyToken = function (req, res, next) {
    var bearerHeader = req.headers['authorization'];
    var token = bearerHeader;
    req.token = token;
    jsonwebtoken_1.default.verify(token, 'privateKey', function (err, authData) {
        if (err) {
            // return res.sendStatus(403);
            res.status(404).json({ success: false, status: 404, message: "Token Not Verified", error_message: err });
        }
        else {
            req.body._id = authData._id;
            next();
        }
    });
};
exports.default = verifyToken;
//# sourceMappingURL=verifytoken.js.map