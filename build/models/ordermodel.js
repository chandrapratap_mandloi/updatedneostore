"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var order = new Schema({
    user_id: {
        type: String,
        ref: "login"
    },
    product_id: {
        type: String,
        ref: "product"
    },
    address_id: {
        type: String,
        ref: "address",
        required: "true"
    },
    qty: {
        type: Number,
        required: true
    },
    total: {
        type: Number,
    },
    subtotal: {
        type: Number,
    },
    gst: {
        type: Number,
    },
    ordertotal: {
        type: Number,
    },
    flag: {
        type: String,
        default: false
    },
    price: {
        type: Number
    },
    orderno: {
        type: String
    },
    orderplaced: {
        type: Date,
        default: Date.now
    }
});
var ordermodel = mongoose_1.default.model('order', order);
exports.default = ordermodel;
//# sourceMappingURL=ordermodel.js.map