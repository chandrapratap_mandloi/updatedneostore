"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var user = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
    },
    gender: {
        type: Number
    },
    phone: {
        type: Number
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    dob: {
        type: String
    },
    profile_img: {
        type: String
    }
});
var userModel = mongoose_1.default.model('user', user);
exports.default = userModel;
//# sourceMappingURL=usermodel.js.map