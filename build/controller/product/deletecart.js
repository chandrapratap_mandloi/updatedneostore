"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for delting address of customer
var cartmodel_1 = __importDefault(require("../../models/cartmodel"));
var deletecart = function (req, res) {
    /**
     * @param _id  user id from token whose address to be deleted
     * @param product_id product id
     * @returns  delete selected product from cart
     */
    console.log('req.params.cart_id', req.params.cart_id);
    console.log('user body', req.body._id);
    cartmodel_1.default.deleteOne({ _id: req.params.cart_id, user_id: req.body._id })
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Product Deleted", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = deletecart;
//# sourceMappingURL=deletecart.js.map