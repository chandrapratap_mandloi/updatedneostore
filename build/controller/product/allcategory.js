"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var categorymodel_1 = __importDefault(require("../../models/categorymodel"));
var allcategory = function (req, res) {
    /**
     * @returns details of all category
     */
    categorymodel_1.default.find()
        .then(function (result) {
        res.status(200).json({ success: 'true', status: 200, product: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err.details });
    });
};
exports.default = allcategory;
//# sourceMappingURL=allcategory.js.map