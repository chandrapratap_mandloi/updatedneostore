"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// this api is used for finding particular product
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var findproduct = function (req, res) {
    /**
     * @param _id id of product to find
     */
    productmodel_1.default.find({ "_id": req.body._id })
        .populate('category_id')
        .populate('color_id')
        .then(function (result) {
        res.status(200).json({ success: 'true', status: 200, product: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err.details });
    });
};
exports.default = findproduct;
//# sourceMappingURL=findproduct.js.map