"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var pdfkit_1 = __importDefault(require("pdfkit"));
var usermodel_1 = __importDefault(require("../../models/usermodel"));
var ordermodel_1 = __importDefault(require("../../models/ordermodel"));
var moment_1 = __importDefault(require("moment"));
var invoice = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var name, phone, table_y, yValue, orderData, data, filename, d, doc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                name = '';
                table_y = [];
                return [4 /*yield*/, ordermodel_1.default.find({ orderno: req.body.order })
                        .populate('product_id')
                        .populate('address_id')];
            case 1:
                orderData = _a.sent();
                return [4 /*yield*/, usermodel_1.default.find({ _id: req.body._id })];
            case 2:
                data = _a.sent();
                name = data[0].first_name + " " + data[0].last_name;
                phone = data[0].phone;
                filename = orderData[0]._id;
                d = new Date();
                filename = "NeoSTORE" + "_" + filename + "_" + "(" + d.toDateString() + ")" + '.pdf';
                doc = new pdfkit_1.default({ margin: 50 });
                generateHeader(doc);
                generateCustomerInformation(doc, orderData, name, phone);
                generateInvoiceTable(doc, orderData, table_y);
                yValue = table_y[table_y.length - 1];
                generateFooter(doc, yValue);
                doc.pipe(fs_1.default.createWriteStream('invoice/' + filename));
                doc.end();
                /*const emailSender = sendmail({
                    silent: false
                });
                const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
                new Promise((resolve, reject) => {
                    emailSender(options, (err, reply) => {
                        if (err || !reply.startsWith("2")) {
                            reject(err);
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
                const output = `<h3> Hi Thankyou for shopping with us</h3>
                <img src="uploads/neosoft.png">
                `;
                sendEmail({
                    from: 'mandloi.parth@gmail.com',
                    to: 'chandrapratap.mandloi@neosofttech.com',
                    subject: 'Invoice Details',
                    html: output,
                    attachments: [
                        {
                          filename: filename}]
                })*/
                console.log(filename, '--------');
                res.status(200).json({ success: true, status_code: 200, message: "Your Invoice of Order No " + orderData[0]._id + " has been generated", receipt: filename });
                return [2 /*return*/];
        }
    });
}); };
var generateHeader = function (doc) {
    doc
        .image("uploads/neo.png", 50, 40, { width: 100, height: 50 })
        .fillColor("#444444")
        // .fillColor("#333333")
        .fontSize(10)
        .text("5th Floor, SIGMA IT PARK", 200, 65, { align: "right" })
        .text("Rabale, MUMBAI, INDIA, 400701", 200, 80, { align: "right" })
        .moveDown();
};
var generateFooter = function (doc, yValue) {
    var footer_y = [];
    for (var i = 0; i < 4; i++) {
        yValue = yValue + 20;
        footer_y.push(yValue);
    }
    doc
        .fontSize(12)
        .text("Terms and conditions apply: The goods sold as are intended for end user consumption and not for re-sale.", 50, footer_y[1])
        .fontSize(11)
        .font('Helvetica-Bold')
        .text("Contact Neostore: 022 4050 0600 || www.neostore.com/helpcentre", 50, footer_y[5], { align: "center" })
        .fillColor('Blue');
    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20;
    doc
        .lineCap('butt')
        .moveTo(50, footer_y[footer_y.length - 1])
        .lineTo(560, footer_y[footer_y.length - 1])
        .stroke();
    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20;
    doc
        .text("Invoice Template by NeoSOFT Technologies", 50, footer_y[footer_y.length - 1])
        .image("uploads/neo.png", 450, footer_y[footer_y.length - 1], { width: 80, height: 40, align: "right" });
};
var generateCustomerInformation = function (doc, orderData, name, phone) {
    var date = moment_1.default(orderData[0].orderplaced).format('DD/MM/YYYY');
    var address = orderData[0].address_id;
    var add = address.address;
    var city = address.city;
    var state = address.state;
    var pin = address.pincode;
    var upadd = add + ' ' + city + ' | ' + state + ' | ' + pin;
    doc
        .fontSize(15)
        .text("Invoice Details", 50, 120)
        .lineCap('butt')
        .moveTo(50, 140)
        .lineTo(560, 140)
        .stroke()
        .fontSize(12)
        .text("ORDER Number: " + orderData[0].orderno, 50, 160)
        .text("ORDER Date: " + date, 50, 180)
        .text("Total Cost: " + orderData[0].ordertotal, 50, 200)
        .fontSize(12)
        .text("Name: " + name, 380, 160)
        .text("Mobile No: " + phone, 380, 180)
        .text("Shipping Address:" + upadd, 380, 200)
        .fontSize(11)
        .lineCap('butt')
        .moveTo(50, 245)
        .lineTo(560, 245)
        .stroke()
        .moveDown();
};
var generateTableRow = function (doc, y, c1, c2, c3, c4, c5) {
    // console.log('Called', y, c1, c2, c3, c4, c5)
    doc
        .fontSize(11)
        .text(c1, 60, y)
        .text(c2, 100, y)
        .text(c3, 280, y, { width: 90, })
        .text(c4, 380, y, { width: 90, })
        .text(c5, 470, y, { width: 90, });
};
var generateInvoiceTable = function (doc, orderData, table_y) {
    var i;
    var invoiceTableTop = 340;
    var sum = 0;
    var position;
    var gst;
    doc
        .fontSize(15)
        .text("Product Details", 50, 280)
        .lineCap('butt')
        .moveTo(50, 300)
        .lineTo(560, 300)
        .stroke()
        .fontSize(13)
        .text("SNO", 50, 320)
        .text("Name", 130, 320)
        .text("Price", 280, 320)
        .text("Quantity", 360, 320)
        .text("Total Price", 450, 320)
        .lineCap('butt')
        .moveTo(50, 340)
        .lineTo(560, 340)
        .stroke();
    for (i = 0; i < orderData.length; i++) {
        var product = orderData[i];
        position = invoiceTableTop + (i + 1) * 20;
        var count = i + 1;
        sum = sum + parseInt(product.total);
        generateTableRow(doc, position, count, orderData[i].product_id.product_name, orderData[i].product_id.product_cost, orderData[i].qty, product.total);
    }
    position = position + 20;
    doc
        .lineCap('butt')
        .moveTo(50, position)
        .lineTo(560, position)
        .stroke();
    for (var j = 0; j < 3; j++) {
        position = position + 20;
        table_y.push(position);
    }
    gst = Math.round((5 * sum) / 100);
    doc
        .fontSize(13)
        .text("Total", 360, table_y[0])
        .text("" + sum, 470, table_y[0])
        .text("GST (5%)", 360, table_y[1])
        .text("" + gst, 470, table_y[1])
        .text("Net Total.", 360, table_y[2])
        .text("" + orderData[0].ordertotal, 470, table_y[2]);
    table_y[table_y.length - 1] = table_y[table_y.length - 1] + 20;
    doc
        .lineCap('butt')
        .moveTo(50, table_y[table_y.length - 1])
        .lineTo(560, table_y[table_y.length - 1])
        .stroke();
};
exports.default = invoice;
//# sourceMappingURL=invoice.js.map