"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//Api for adding new color
var colormodel_1 = __importDefault(require("../../models/colormodel"));
var color = function (req, res) {
    /**
     * @param color_name  name of color
     * @param color_code color code
     */
    var newData = new colormodel_1.default({
        color_name: req.body.color_name,
        color_code: req.body.color_code
    });
    newData.save()
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Color Added", color: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = color;
//# sourceMappingURL=color.js.map