"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var usermodel_1 = __importDefault(require("../../models/usermodel"));
var getuser = function (req, res) {
    usermodel_1.default.find({ "_id": req.body._id })
        /**
         * @param _id id of user from token
         * @returns details of user
         */
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "User Details", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = getuser;
//# sourceMappingURL=getuser.js.map