"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Api for adding address
var addressmodel_1 = __importDefault(require("../../models/addressmodel"));
/**
 *
 * @param customer_id     customer id from token
 * @param address           address of customer
 * @param pincode           pincode of area
 * @param city              city
 * @param state             state
 * @param country           country
 *
 */
var addaddress = function (req, res) {
    var newData = new addressmodel_1.default({
        customer_id: req.body._id,
        address: req.body.address,
        pincode: req.body.pincode,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country,
    });
    newData.save()
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Address Added", user: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
    // addressmodel.find({"customer_id":req.body._id})
    // .then(previous=>{
    //     let newData = new addressmodel({
    //         customer_id:req.body._id,
    //         address:req.body.address,
    //         pincode:req.body.pincode,
    //         city:req.body.city,
    //         state:req.body.state,
    //         country:req.body.country,      
    //     })      
    //     newData.save()
    //         .then(result=>{
    //             res.status(200).json({success:true,status:200,message:"Address Added",user:result,data:previous})
    //         })
    //         .catch(err=>{
    //             res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    //         })
    // })
    // .catch(err=>{
    //     res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    // })
};
exports.default = addaddress;
//# sourceMappingURL=address.js.map