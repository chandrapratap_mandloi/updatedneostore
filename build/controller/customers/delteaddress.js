"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for delting address of customer
var addressmodel_1 = __importDefault(require("../../models/addressmodel"));
var deleteaddress = function (req, res) {
    /**
     * @param _id  user id from token whose address to be deleted
     * @param address_id address id
     * @returns  delete selected address of user
     */
    console.log('hii');
    console.log('address id ==>', req.params.address_id, 'customer_id==>', req.body._id);
    addressmodel_1.default.deleteOne({ _id: req.params.address_id, customer_id: req.body._id })
        // addressmodel.remove( {"_id": req.body.address_id})
        .then(function (result) {
        console.log('address id ==>', req.params.address_id, 'customer_id==>', req.body._id);
        res.status(200).json({ success: true, status: 200, message: "Address Deleted", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = deleteaddress;
//# sourceMappingURL=delteaddress.js.map