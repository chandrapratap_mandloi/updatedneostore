// api for delting address of customer
import taskmodel from '../../models/newTaskmodel'
// import Joi from 'joi'
import {Request,Response} from 'express'
const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);
const OutTime = (req:Request,res:Response)=>{   

    const schema = Joi.object().keys({
        userid: Joi.string().required(),
        date:Joi.date().format('YYYY-MM-DD')
    })
    Joi.validate(req.body,schema,function (err:any, result:any){
        if(err)
        {
           return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        }
        else{
            // taskmodel.find({"userid":req.body.userid,date:new Date(req.body.date).toString()})
            taskmodel.aggregate(
                [
                {
                    $match:{
                        // $and:[{
                            "date" : { 
                            // $eq: new Date(req.body.date), 
                            // $lt: new Date(req.body.date), 
                            $gte: new Date(new Date().setDate(new Date(req.body.date).getDate()-1))
                        }
                    // }]
                }
                },
                  {
                    $group:
                      {
                        _id: "$userid",
                        SignInTime: { $first: "$date" },
                        SignOutTime: { $last: "$date" }
                      }
                  }
                ]
             )
            .then(result=>{
                if(result.length==0)
                {
                res.status(404).json({success:false,message:'No User Found'})
                }
                else{
                    res.status(200).json({success:true,status:200,product:result})
                }
            })
            .catch(err=>{
                res.status(404).json({success:false,status:404,err:err.details});
            })
        }
    })
}
export default OutTime
