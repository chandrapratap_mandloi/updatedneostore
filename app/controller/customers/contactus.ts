// api is used for adding new product
import contactmodel from '../../models/contactmodel'
import {Request,Response} from 'express'
import Joi from 'joi'
const contactus = (req:Request,res:Response)=>{


    /**
     * @param name 
     * @param email 
     * @param phone 
     * @param subject 
     * @param message
     */
    

    const schema = Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        phone: Joi.number().required(),
        subject: Joi.string().min(3).required(),
        message: Joi.string().min(5).required(),
        name: Joi.string().min(3).required()
    })
    Joi.validate(req.body,schema,function (err:any, result:any){
        if(err)
        {
           return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        }
        else{
            let newData = new contactmodel({
                name:req.body.name,
                email:req.body.email,
                phone:req.body.phone,
                subject:req.body.subject,
                message:req.body.message
            })
            newData.save()
            .then(result=>{
                res.status(200).json({success:true,status:200,message:"Thanks for contacting us! We will be in touch with you shortly."})
            })
            .catch(err=>{
                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
            })
        }
    })
    
 
    
}
export default contactus
