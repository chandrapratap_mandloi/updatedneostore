var _ = require("lodash");
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
import moment from "moment";
var pdfCount = 0,
excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');


const BrandController = {
getBrandById: async (req, res) => {
try {
const brand = await BrandService.getBrandById(req.params.brand_id);
logger.info(" Getting brand by id = " + req.params.brand_id);
res.send({
code: 200,
status: "success",
message: "List the brand",
data: brand
});
} catch (error) {
logger.error("Error in getting brand :" + error);
res.status(400).send({
code: 400,
status: "error",
message: "Error in getting brand",
data: {}
});
}
},
createBrand: async (req, res) => {
try {
let url;
let bodyData = JSON.parse(JSON.stringify(req.body));
let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
if (req.files && req.files.url) {
let imageFile = req.files.url;
if (imageFile) {
let extension = imageFile.mimetype.split("/")[1];

let filePath = MEDIA.local_file_path + "brand_image" + "/";
let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
url = "/static/brand_image" + "/" + file_name;
if (!MEDIA.useS3) {
await moveFile(imageFile, filePath, file_name)
.catch((error) => {
console.log(error)
imageError = true;
return badResponse(res, "Image Upload Failed");
});
}
}
}
let brand ={};
brand.category_id = data.category_id;
brand.brand_name = data.brand_name;
brand.description = data.description;
brand.url = url;
let savedBrand = await BrandService.createBrand(brand);
if (savedBrand.url && savedBrand.url !== "") {
savedBrand.url = config.api_end_point + savedBrand.url
} else {
savedBrand.url = ""
}

logger.info("New brand is created");
return res.send({
code: 200,
status: "success",
message: "New brand is created",
data: savedBrand
});
}
catch (error) {
console.log(error)
logger.error("Error in Creating brand :" + error);
res.status(400).send({
code: 400,
status: "error",
message: "Error in Creating brand",
data: []
});
}
},
updateBrand: async (req, res) => {
try {
let url;
let bodyData = JSON.parse(JSON.stringify(req.body));
let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
if (req.files && req.files.url) {
let imageFile = req.files.url;
if (imageFile) {
let extension = imageFile.mimetype.split("/")[1];

let filePath = MEDIA.local_file_path + "brand_image" + "/";
let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
url = "/static/brand_image" + "/" + file_name;
if (!MEDIA.useS3) {
await moveFile(imageFile, filePath, file_name)
.catch((error) => {
console.log(error)
imageError = true;
return badResponse(res, "Image Upload Failed");
});
}
}
}
let brand ={};
brand.category_id = data.category_id;
brand.brand_name = data.brand_name;
brand.description = data.description;
brand.url = url;
let savedBrand = await BrandService.updateBrand(brand);
if (savedBrand.url && savedBrand.url !== "") {
savedBrand.url = config.api_end_point + savedBrand.url
} else {
savedBrand.url = ""
}
logger.info(" brand is update");
return res.send({
code: 200,
status: "success",
message: "brand is update",
data: editedCategory
});
} catch (error) {
console.log(error)
logger.error("Error in brand Category :" + error);
res.status(400).send({
code: 400,
status: "error",
message: "Error in brand Category",
data: {}
});
}
},
getAllBrand: async (req, res) => {
try {
var search;
if (_.isEmpty(req.query.searchkey)) {
search = '';
} else {
search = req.query.searchkey
}
let limit = Number(req.query.limit) ;
let offset = parseInt(req.query.skip);
let skip = (offset - 1) * limit;
const brandData = await BrandService.getAllBrand(search, limit, skip);
logger.info(" Getting all brands");
res.send({
code: 200,
status: "success",
message: "Listing All brands",
data: brandData
});
} catch (error) {
logger.error("Error in getting brands :" + error);
res.status(400).send({
code: 400,
status: "error",
message: "Error in getting brands record",
data: []
});
}
},
// searchProductCategory: async(req,res )=>{
// try {
// var searchByName = (_.isEmpty(req.query.searchName)) ? '' : req.query.searchName;
// let limit = Number(req.query.limit) || 10;
// let offset = (req.query.page) ? parseInt(req.query.page) : 1
// let skip = (offset - 1) * limit;
// console.log(searchByName,limit, skip);
// let category_id = req.params.category_id;
// let product = await BrandService.searchProductCategory(searchByName,category_id,limit, skip);
// logger.info(" getting product by categoey");
// res.send({
// code: 200,
// status: "success",
// message: "product found",
// data: product
// });

// } catch (error) {
// logger.error(" Getting error"+error);
// res.status(400).send({
// code: 400,
// status: "error",
// message: "Something went wrong..",
// data: {}
// });
// }
// },
// getBrandNameById : async (req,res) =>{
// try {
// let category_id = req.params.category_id;
// let brands = await BrandService.getBrandNameById(category_id);
// logger.info(" Getting brand");
// if(brands){
// res.send({
// code: 200,
// status: "success",
// message: "list of brands",
// data: brands
// });
// }else{
// res.send({
// code: 204,
// status: "success",
// message: "No data found",
// data: []
// });
// }
// } catch (error) {
// logger.error(" Getting error"+error);
// res.status(400).send({
// code: 400,
// status: "error",
// message: "Something went wrong..",
// data: {}
// });
// }
// },
deleteBrand: async (req, res) => {
try {
var ids = (req.params.brand_id).split(',');
console.log(ids)
var data = await BrandService.deletebrandByIds(ids);
res.send({
code: 200,
status: "success",
message: "brand is successfully deleted",
data: data
});
} catch (error) {
console.log(error)
res.status(400).send({
code: 400,
status: "error",
message: "brand does not delete",
data: {}
});

}
}
};
let badResponse = function (res, msg) {
res.status(400).send({
code: 400,
status: "error",
message: msg,
data: []
});
};
let moveFile = async (media_file, filePath, file_name) => {
return await new Promise((resolve, reject) => {

mkdirp(filePath, function (err) {
if (err) {
logger.error(err);
reject({
code: 400,
status: "error",
message: "Error in Uploading file",
data: []
});
} else {
media_file.mv(filePath + file_name, function (err) {
if (err) {
logger.error(err);
reject({
code: 400,
status: "error",
message: "Error in Uploading file",
data: []
});
} else {
logger.log("File Moved")
resolve({
code: 200,
status: "success",
message: "Uploaded Successfully",
data: []
})
}
});
}
});
});
}

export default BrandController;



