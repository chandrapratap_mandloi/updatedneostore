// api for edit address of user
import addressmodel from '../../models/addressmodel'
import {Request,Response} from 'express'

const editaddress = (req:Request,res:Response)=>{   

    /**
     * @param _id  id of user from token
     * @param address
     * @param pincode
     * @param city
     * @param state
     * @param country
     * @returns updates address of user
     */
    addressmodel.update(
        { customer_id: req.body._id ,_id:req.body.address_id},
        {
          $set: {
            'address':req.body.address,
            'pincode':req.body.pincode,
            'city':req.body.city,
            'state':req.body.state,
            'country':req.body.country
          }
        }
     )
     .then(result=>{
      addressmodel.find({_id:req.body.address_id})
      .then(result=>{
        res.status(200).json({success:true,status:200,message:"Address Updated",address:result})
      })
      .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
      })
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default editaddress