// api for delting address of customer
import addressmodel from '../../models/addressmodel'
import {Request,Response} from 'express'

const deleteaddress = (req:Request,res:Response)=>{   

    /**
     * @param _id  user id from token whose address to be deleted
     * @param address_id address id
     * @returns  delete selected address of user
     */
    console.log('hii')
    console.log('address id ==>',req.params.address_id,'customer_id==>',req.body._id)
    addressmodel.deleteOne({_id:req.params.address_id, customer_id: req.body._id})
    // addressmodel.remove( {"_id": req.body.address_id})
   .then(result=>{
       console.log('address id ==>',req.params.address_id,'customer_id==>',req.body._id)
        res.status(200).json({success:true,status:200,message:"Address Deleted",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default deleteaddress