//api for editing profile of user
import userModel from '../../models/usermodel'
import {Request,Response} from 'express'

const editprofile = (req:Request,res:Response)=>{
        
        /**
         * @param first_name
         * @param last_name
         * @param gender
         * @param dob
         * @param phone
         * @param email
         * @param profile_img
         * @returns update profile of user
         */
    console.log(req.file,'+++++======')
         if(!req.file)
         {
             console.log('not file')
                return res.status(404).json({success:false,message:"Add profile picture"})
         }
         else{
            userModel.findByIdAndUpdate(req.body._id, {$set: {first_name:req.body.first_name,last_name:req.body.last_name,dob:req.body.dob,phone:req.body.phone,gender:req.body.gender,email:req.body.email,profile_img:req.file.filename}}) 
            // ,'profile_img':req.file.filename
            .then(result=>{
                let updated ={first_name:req.body.first_name, last_name: req.body.last_name,dob:req.body.dob,phone:req.body.phone,gender:req.body.gender,email:req.body.email,profile_img:req.file.filename}
                console.log(updated)
                res.status(200).json({success:true,status:200,message:"Profile Updated",customer:updated})
            })
            .catch(err=>{
                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
            })
        }  
}
export default editprofile