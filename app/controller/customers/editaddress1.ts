// db.my_collection.update(
//     {_id : "document_id"},
//     {$set : {"my_array.1.content" : "New content B"}}
// )
// Api for adding address
import newaddressmodel from '../../models/newaddressmodel'
import {Request,Response} from 'express'

const editaddress1 = (req:Request,res:Response)=>{
    console.log('api hit')
                newaddressmodel.update(
                        {
                            customer_id : req.body._id,
                            address1: { $elemMatch: { _id: req.body.address_id } }
                        },
                        {$set : {
                            "address1.$.address":req.body.address,
                            "address1.$.pincode":req.body.pincode,
                            "address1.$.city":req.body.city,
                            "address1.$.state":req.body.state,
                            "address1.$.country":req.body.country
                        }}
                    )
            .then(result=>{
                res.status(200).json({success:true,status:200,message:"Address updated",address:result})
              })
              .catch(err=>{
                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
              })
}      
export default editaddress1