import userModel from '../../models/usermodel'
import {Request,Response} from 'express'

const getuser = (req:Request,res:Response)=>{
    userModel.find({"_id":req.body._id})
    /**
     * @param _id id of user from token
     * @returns details of user
     */
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"User Details",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default getuser