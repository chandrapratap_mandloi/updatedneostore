import newaddressmodel from '../../models/newaddressmodel'
import {Request,Response} from 'express'

const getuseraddress = (req:Request,res:Response)=>{
    newaddressmodel.find({"customer_id":req.body._id})
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"User Details",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default getuseraddress