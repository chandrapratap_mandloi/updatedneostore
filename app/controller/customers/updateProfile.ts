//api for editing profile of user
import userModel from '../../models/usermodel'
import {Request,Response} from 'express'
var base64ToImage = require('base64-to-image');
var fs = require("fs");

const updateProfile = (req:Request,res:Response)=>{
    var base64Str = req.body.profile_img;
    var path ='./uploads/profile_image/';   
    var optionalObj = {'fileName': 'image'+Date.now(), 'type':'jpeg'};;
    
    var imageInfo = base64ToImage(base64Str, path, optionalObj); 
    var filepath =req.protocol + '://' + req.headers.host  + '/profile_image/' +imageInfo.fileName  

    console.log('test')

    userModel.findByIdAndUpdate(req.body._id, {$set: {first_name:req.body.first_name,last_name:req.body.last_name,dob:req.body.dob,phone:req.body.phone,gender:req.body.gender,email:req.body.email,profile_img:req.body.profile_img,user_img:filepath}}) 
    .then(()=>{
        let updated ={first_name:req.body.first_name, last_name: req.body.last_name,dob:req.body.dob,phone:req.body.phone,gender:req.body.gender,email:req.body.email,profile_img:req.body.profile_img,user_img:filepath}
        res.status(200).json({success:true,status:200,message:"Profile Updated",customer:updated})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
    
export default updateProfile