// for user login

import userModel from '../../models/usermodel'
import { Request, Response } from 'express'
import bcrypt from 'bcrypt'
import Joi from "joi";

const resetPassword = (req: Request, res: Response) => {

    let id: number
            console.log(req.body,'========')
            userModel.findOne({ _id: req.body._id })
                .then(result => {
                    if (result == null) {
                        console.log('if')
                        return res.status(404).json({ status_code: 404, success: false, message: "User not found"})
                    }
                    else {
                        console.log('inside else.......',result)
                        const schema = Joi.object().keys({
                            password: Joi.string().required(),
                            newpassword:Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
                            conpassword: Joi.any()
                            .equal(Joi.ref("newpassword"))
                            .required(),
                            _id:Joi.any()
                        });
                        Joi.validate(req.body, schema, (err: any, value: string) => {
                            if (err) {
                              return res
                                .status(400)
                                .json({
                                  err,
                                  code: 400,
                                  message: "joi validation error",
                                  error_message: err
                                });
                            } else {
                                const { password, ...user } = result.toObject(); //{expiresIn:'30s'},
                                if (bcrypt.compareSync(req.body.password, password)) {
                                    console.log('password match')
                                    const saltRounds = 10;
                                    const myPlaintextPassword = req.body.newpassword;
                                    const salt = bcrypt.genSaltSync(saltRounds);
                                    const passwordHash = bcrypt.hashSync(myPlaintextPassword, salt);
                                    console.log(passwordHash,'===+++++')
                                    userModel.findByIdAndUpdate(req.body._id, {
                                        password: passwordHash,
                                    })
                                
                                        .then(result => {
                                            res.status(200).json({ status_code: 200, success: true, message: "Password Updated Successfully"})
                                        })
                                        .catch(err => {
                                            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                                        })
                                }
                                else {
                                    console.log('password not match')
                                    res.status(404).json({ status_code: 404, success: false, message: "Password not match" })
                                }
                            }
                        

                    })
                }
                })
                .catch(err => {
                    res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                })
    }


export default resetPassword