// Api for adding address
import newaddressmodel from '../../models/newaddressmodel'
import {Request,Response} from 'express'

const deleteaddress1 = (req:Request,res:Response)=>{
                newaddressmodel.update(
                    {
                      "customer_id" : req.body._id,
                    },
                    {
                      "$pull" : { "address1" : { "_id" : req.params.address_id } }
                    }
                )
            .then(result=>{
                res.status(200).json({success:true,status:200,message:"Address Deleted",address:result})
              })
              .catch(err=>{
                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
              })
}      
export default deleteaddress1