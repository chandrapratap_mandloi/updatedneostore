// api for registring new user
import { Request, Response, response } from 'express'
import sendmail from 'sendmail'

const SendReport = (req: Request, res: Response) => {
    console.log(req.body)
    const length = req.body.userTask.length
    try{
      const emailSender = sendmail({
        silent: false
    });
    
    
    const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
    new Promise((resolve, reject) => {
        emailSender(options, (err, reply) => {
            if (err || !reply.startsWith("2")) {
                reject(err);
            } 
            else {
                resolve(true);
            }
        });
    });
    // let htmlcontent=
    let dynamicContents = "";
    for(var i=0; i<length;i++)
      {
        dynamicContents +=
        `
        <tr>
          <td>${req.body.userTask[i].userName}</td>
          <td>${req.body.userTask[i].task}</td>
          <td>${req.body.userTask[i].issues}</td>
          <td>${req.body.userTask[i].help} </td>
          <td>${req.body.userTask[i].adminComment===undefined ? 'No Comment' : req.body.userTask[i].adminComment}</td>
        </tr>
      `
  }

  let output = `<!DOCTYPE html>
  <html>
  
  <head>
      <style>
          .imagediv{
            background:
            linear-gradient(red, transparent),
            linear-gradient(to top left, lime, transparent),
            linear-gradient(to top right, blue, transparent);
            background-blend-mode: screen;
              height: 124px;
              display: flex;
              justify-content: center;
              margin-bottom: 2%;
          }
          h3{
              
              color: black;
              height: 25px;
          }
          table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
          }
  
          td,
          th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
          }
  
          tr:nth-child(even) {
              background-color: #dddddd;
          }
      </style>
  </head>
  
  <body>
      <div class="imagediv">
              <h3> Below are the details of Sprint meeting for today : </h3>
      </div>
      <table>
          <tr style="background-color: rgb(29, 137, 228); color: white">
              <th>Name</th>
              <th>Task</th>
              <th>Issues</th>
              <th>Priority</th>
              <th>SMaster Comment</th>
          </tr>
          ${dynamicContents}
          
      </table>
  </body>
  
  </html>`
    sendEmail({
        from: 'chandrapratap.mandloi@neosofttech.com',
        to: req.body.to,
        cc:req.body.cc,
        bcc:req.body.bcc,
        // to: 'chandrapratap.mandloi@neosofttech.com',
        subject: 'MOM - Rabale team',
        html: output,
    })
   
    .then(result=>{
        console.log('inside then')
        return res.status(200).json({ success: true, status: 200, message: "Mail Sent" })
   })
   .catch(err=>{
    console.log('error****',err)
 })

    }
    catch (err) {
      console.log(err);
      // ResponseHandler.JSONERROR(req, res, "addTask");
    }
    
}
export default SendReport