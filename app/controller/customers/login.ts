import userModel from '../../models/usermodel'
import {Request,Response} from 'express'
import Joi from 'joi'
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken'
import addressmodel from '../../models/addressmodel'
import cartmodel from '../../models/cartmodel'
const login = (req:Request,res:Response)=>{
    const schem = Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(4).required(),
        customer_id:Joi.string()
    })
    Joi.validate(req.body,schem,function (err:any, result:any){
        if(err)
        {
           return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        }
        else{
            userModel.find({"email":req.body.email},(err:any, result:any)=>{
                if(err)
                {
                    res.status(204).json({success:false,status:204,err:err.details});
                }
                if(result.length == 0)
                {
                    res.status(404).json({success:false,status:404, message: "Enter Correct E-Mail" })
                }
                else{
                    /**
                     * @param email 
                     * @param password
                     * @returns details of user and address of user
                     */
                    const {password, ...users} =result[0].toObject()
                    if (bcrypt.compareSync(req.body.password, password)) {
                        jwt.sign({ email: req.body.email,_id:result[0]._id }, 'privateKey', function(err:any, token:any) {
                            addressmodel.find({"customer_id":users._id})
                            .then(result=>{
                                console.log(users._id,"jfoghf")
                                cartmodel.find({user_id:users._id,flag:false}).select('-flag')
                                .populate('product_id')
                                .then(results=>{
                                    res.status(200).json({success:true,status:200,Message:"Login Successfully",token: token,cart_count:results.length,cart:results,data: users,address:result})
                                })
                                .catch(err=>{
                                    res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
                                })
                            })
                            .catch(err=>{
                                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
                            })
                        });
                    } else {
                        res.status(404).json({success:false,status:404, message: "Password not Match" })
                    }
                }
            })
        }
    })
}
export default login
