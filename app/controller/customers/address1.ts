// Api for adding address
import newaddressmodel from '../../models/newaddressmodel'
import {Request,Response} from 'express'

/**
 * 
 * @param customer_id     customer id from token
 * @param address           address of customer
 * @param pincode           pincode of area
 * @param city              city
 * @param state             state
 * @param country           country
 * 
 */
const addaddress1 = (req:Request,res:Response)=>{
    const id='5dd2864d9c561413564765b6'

    let newData = new newaddressmodel({
        customer_id:req.body._id,
        address1:[
            {
                address:req.body.address,
                pincode:req.body.pincode,
                city:req.body.city,
                state:req.body.state,
                country:req.body.country, 
            }
        ]     
    })
    newaddressmodel.find({customer_id:req.body._id})
        .then(result=>{
            if(result.length===0)
            {
                console.log('inside if',result.length)
                newData.save()
                res.status(200).json({success:true,status:200,message:"Address Added",user:result})
            }
            else{
                console.log('inside else')
                newaddressmodel.update({customer_id: req.body._id},{
                    $push:{
                            address1:{
                                address:req.body.address,
                                pincode:req.body.pincode,
                                city:req.body.city,
                                state:req.body.state,
                                country:req.body.country
                            }
                        }
            })
            .then(result=>{
                res.status(200).json({success:true,status:200,message:"Address Updated",address:result})
              })
              .catch(err=>{
                res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
              })
            }
        })
        .catch(err=>{
            res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        })
}      
export default addaddress1