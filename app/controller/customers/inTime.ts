// taskmodel
// api for registring new user
import taskmodel from '../../models/newTaskmodel'
import{Request, Response, response} from 'express'


const inTime = (req:Request,res:Response)=>{
                let newData = new taskmodel({
                    userid:req.body.userid,
                    date:Date.now()    
                })
                newData.save()
                .then(result=>{
                   return res.status(200).json({success:true,status:200,message:"Customer Registered Successfully",user:result})
                })
                .catch(err=>{
                return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
                })     
    }
export default inTime