// api for registring new user
import userModel from '../../models/usermodel'
import{Request, Response, response} from 'express'
import Joi, { string } from 'joi'
import bcrypt from 'bcrypt';
import sendmail from 'sendmail'
// var twilio = require('twilio');
import twilio from 'twilio'
// var client = new twilio('TWILIO_ACCOUNT_SID', 'TWILIO_AUTH_TOKEN');


const registerUser = (req:Request,res:Response)=>{

   /**
    * @param email
    * @param password     password must be minimum 4 characters long and 8 maximum characters long and should contains at least one capital letter, one small letter and one digit
    * @param cnfpassword  confirm password
    * @param gender       numeric in form of 0 or 1, 1 for male and 0 for female
    * @param phone
    * @param first_name
    * @param last_name
    */

    const schema = Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().regex(/^(?=.*[a-zA-Z])(?=.*[0-9])/).min(4).max(20).required(),
        cnfpassword: Joi.string().equal(Joi.ref('password')).required(),
        gender: Joi.string().required(),
        phone: Joi.number().required(),
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
    })
    Joi.validate({email:req.body.email,password:req.body.password,cnfpassword:req.body.cnfpassword,gender:req.body.gender,phone:req.body.phone,first_name:req.body.first_name,last_name:req.body.last_name},schema,function (err:any, result:any){
        if(err)
        {
                return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        }
        else{
            //  for converting password to hash and send welcome mail to user
            const saltRounds = 10
            const hash = req.body.password
            const salt = bcrypt.genSaltSync(saltRounds)
            const passwordHsh = bcrypt.hashSync(hash, salt)
            console.log('hashed', passwordHsh)
            console.log("email:",req.body.email)
               
                // client.messages.create({
                //     to: req.body.phone,
                //     from: '8319462105',
                //     body: 'Hello from Neostore!'
                //   });
                let newData = new userModel({
                    email:req.body.email,
                    password:passwordHsh,
                    cnfpassword:req.body.cnfpassword,
                    gender:req.body.gender,
                    phone:req.body.phone,
                    first_name:req.body.first_name,
                    last_name:req.body.last_name,     
                })
                newData.save()
                .then(result=>{
                    const emailSender = sendmail({
                        silent: false
                    });
                    const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
                    new Promise((resolve, reject) => {
                        emailSender(options, (err, reply) => {
                            if (err || !reply.startsWith("2")) {
                                reject(err);
                            } 
                            else {
                                resolve(true);
                            }
                        });
                    });
                    const output = `<h3> Hi ${req.body.email} You are successfully registered with NeoStore: </h3>
                    <img src="uploads/neosoft.png">
                    `;
                    sendEmail({
                        from: 'chandrapratap.mandloi@neosofttech.com',
                        to: req.body.email,
                        subject: 'Welcome to NeoStore',
                        html: output,
                    })
                   return res.status(200).json({success:true,status:200,message:"Customer Registered Successfully",user:result})
                })
                .catch(err=>{
            userModel.find({email:req.body.email})
            .then(result=>{
                return res.status(404).json({success:false,status:404,message:"Email already exist"})
             })
             .catch(err=>{
                return res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})     
             })
                })
        }      
    })
}
export default registerUser