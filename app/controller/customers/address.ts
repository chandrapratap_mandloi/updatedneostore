// Api for adding address
import addressmodel from '../../models/addressmodel'
import {Request,Response} from 'express'

/**
 * 
 * @param customer_id     customer id from token
 * @param address           address of customer
 * @param pincode           pincode of area
 * @param city              city
 * @param state             state
 * @param country           country
 * 
 */
const addaddress = (req:Request,res:Response)=>{


    let newData = new addressmodel({
        customer_id:req.body._id,
        address:req.body.address,
        pincode:req.body.pincode,
        city:req.body.city,
        state:req.body.state,
        country:req.body.country,      
    })
                   
    newData.save()
        .then(result=>{
            res.status(200).json({success:true,status:200,message:"Address Added",user:result})
        })
        .catch(err=>{
            res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
        })



    // addressmodel.find({"customer_id":req.body._id})
    // .then(previous=>{
    //     let newData = new addressmodel({
    //         customer_id:req.body._id,
    //         address:req.body.address,
    //         pincode:req.body.pincode,
    //         city:req.body.city,
    //         state:req.body.state,
    //         country:req.body.country,      
    //     })      
    //     newData.save()
    //         .then(result=>{
    //             res.status(200).json({success:true,status:200,message:"Address Added",user:result,data:previous})
    //         })
    //         .catch(err=>{
    //             res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    //         })
    // })
    // .catch(err=>{
    //     res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    // })
}      
export default addaddress