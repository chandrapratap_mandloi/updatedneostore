// Api for getting all colors
import colormodel from '../../models/colormodel'
import {Request,Response} from 'express'

const allcolor = (req:Request,res:Response)=>{

    colormodel.find()
    .then(result=>{
        res.status(200).json({success:true,status:200,colors:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,err:err.details});
    })
    
}
export default allcolor
