// api for getting specific color details
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const getcolor = (req:Request,res:Response)=>{

    /**
     * @param color_id 
     */
    productmodel.find({"color_id":req.body.color_id})
    .populate('color_id')
    .then(result=>{
        res.status(200).json({success:true,status:200,product:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,err:err.details});
    })
    
}
export default getcolor
