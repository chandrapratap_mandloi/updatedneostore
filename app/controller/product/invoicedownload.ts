//  this api is used for getting order details of user
import cartmodel from '../../models/cartmodel'
import ordermodel from '../../models/ordermodel'
import {Request,Response} from 'express'
import path from 'path'
const downloadinvoice = (req:Request,res:Response)=>{
    console.log('aPI called',req.params)
        try {
        var fileName = req.params.filename
        const filePath = path.join(__dirname + "../../../../invoice/", fileName)
        console.log(filePath,'<=====filepath')
        res.download(filePath)
        } catch (error) {
        console.log(error)
        res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
        });
        }
        }
// }
export default downloadinvoice