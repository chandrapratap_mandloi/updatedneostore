import { Request, Response } from 'express'
import fs from 'fs'
import PDFDocument from 'pdfkit'
import userModel from '../../models/usermodel'
import ordermodel from '../../models/ordermodel'
import moment from 'moment'
import sendmail from 'sendmail'


const invoice = async (req: Request, res: Response) => {

    let name: string = ''
    let phone: any
    let table_y: any = []
    let yValue: any
    let orderData = await ordermodel.find({orderno:req.body.order})
    .populate('product_id')
    // .populate('address_id')
    let data = await userModel.find({ _id: req.body._id })
    name = data[0].first_name + " " + data[0].last_name
    phone = data[0].phone
    let filename = orderData[0]._id

    let d = new Date();

    filename = "NeoSTORE" + "_" + filename + "_" + "(" + d.toDateString() + ")" + '.pdf'

    let doc = new PDFDocument({ margin: 50 })

    generateHeader(doc);
    generateCustomerInformation(doc, orderData, name, phone);
    generateInvoiceTable(doc, orderData, table_y);
    yValue = table_y[table_y.length - 1]
    generateFooter(doc, yValue);
    doc.pipe(fs.createWriteStream('invoice/' + filename));
    doc.end();
    // var filepath =req.protocol + '://' + req.headers.host  + '/profile_image/' +imageInfo.fileName  
    res.status(200).json(
        { 
            success: true, status_code: 200, 
            message: `Your Invoice of Order No ${orderData[0]._id} has been generated`, 
            // receipt: filename ,
            receipt:`${req.protocol}://${req.headers.host}/download/${filename}`
        })
}

const generateHeader = (doc: any) => {

    doc
        .image("uploads/neo.png", 50, 40, { width: 100, height: 50 })
        .fillColor("#444444")
        // .fillColor("#333333")
        .fontSize(10)
        .text("5th Floor, SIGMA IT PARK", 200, 65, { align: "right" })
        .text("Rabale, MUMBAI, INDIA, 400701", 200, 80, { align: "right" })
        .moveDown();
        
}

const generateFooter = (doc: any, yValue: any) => {
    let footer_y: any = []

    for (let i = 0; i < 4; i++) {
        yValue = yValue + 20
        footer_y.push(yValue)
    }

    doc
        .fontSize(12)
        .text("Terms and conditions apply: The goods sold as are intended for end user consumption and not for re-sale.",50, footer_y[1])
        
        .fontSize(11)
        .font('Helvetica-Bold')
        .text("Contact Neostore: 022 4050 0600 || www.neostore.com/helpcentre", 50, footer_y[5], { align: "center" })
        .fillColor('Blue')
        
    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20

    doc
        .lineCap('butt')
        .moveTo(50, footer_y[footer_y.length - 1])
        .lineTo(560, footer_y[footer_y.length - 1])
        .stroke()

    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20

    doc
        .text("Invoice Template by NeoSOFT Technologies", 50, footer_y[footer_y.length - 1])
        .image("uploads/neo.png", 450, footer_y[footer_y.length - 1], { width: 80, height: 40, align: "right" })
}

const generateCustomerInformation = (doc: any, orderData: any, name: any, phone: any) => {
    let date = moment(orderData[0].orderplaced).format('DD/MM/YYYY')
    let address = orderData[0].address_id
    let add = orderData[0].address
    let city = orderData[0].city
    let state = orderData[0].state
    let pin = orderData[0].pincode
    let upadd = add +' '+ city +' | '+ state +' | '+ pin
    doc
        .fontSize(15)
        .text(`Invoice Details`, 50, 120)
        .lineCap('butt')
        .moveTo(50, 140)
        .lineTo(560, 140)
        .stroke()
        .fontSize(12)
        .text(`ORDER Number: ${orderData[0].orderno}`, 50, 160)
        .text(`ORDER Date: ${date}`, 50, 180)
        .text(`Total Cost: ${orderData[0].ordertotal}`, 50, 200)
        .fontSize(12)
        .text(`Name: ${name}`, 380, 160)
        .text(`Mobile No: ${phone}`, 380, 180)
        .text(`Shipping Address:${upadd}`, 380, 200)
        .fontSize(11)
        .lineCap('butt')
        .moveTo(50, 245)
        .lineTo(560, 245)
        .stroke()
        .moveDown();

}

const generateTableRow = (doc: any, y: any, c1: any, c2: any, c3: any, c4: any, c5: any) => {
    // console.log('Called', y, c1, c2, c3, c4, c5)
    doc
        .fontSize(11)
        .text(c1, 60, y)
        .text(c2, 100, y)
        .text(c3, 280, y, { width: 90, })
        .text(c4, 380, y, { width: 90, })
        .text(c5, 470, y, { width: 90, })

}

const generateInvoiceTable = (doc: any, orderData: any, table_y: any) => {
    let i
    let invoiceTableTop: number = 340
    let sum: number = 0
    let position: any
    let gst

    doc
        .fontSize(15)
        .text(`Product Details`, 50, 280)
        .lineCap('butt')
        .moveTo(50, 300)
        .lineTo(560, 300)
        .stroke()
        .fontSize(13)
        .text(`SNO`, 50, 320)
        .text(`Name`, 130, 320)
        .text(`Price`, 280, 320)
        .text(`Quantity`, 360, 320)
        .text(`Total Price`, 450, 320)
        .lineCap('butt')
        .moveTo(50, 340)
        .lineTo(560, 340)
        .stroke()

    for (i = 0; i < orderData.length; i++) {
        const product = orderData[i];
        position = invoiceTableTop + (i + 1) * 20;
        let count = i + 1
        sum = sum + parseInt(product.total)

        generateTableRow(
            doc,
            position,
            count,
            orderData[i].product_id.product_name,
            orderData[i].product_id.product_cost,
            orderData[i].qty,
            product.total,
        );
    }

    position = position + 20
    doc
        .lineCap('butt')
        .moveTo(50, position)
        .lineTo(560, position)
        .stroke()

    for (let j = 0; j < 3; j++) {
        position = position + 20;
        table_y.push(position)
    }

    gst = Math.round((5 * sum) / 100)

    doc
        .fontSize(13)
        .text(`Total`, 360, table_y[0])
        .text(`${sum}`, 470, table_y[0])
        .text(`GST (5%)`, 360, table_y[1])
        .text(`${gst}`, 470, table_y[1])
        .text(`Net Total.`, 360, table_y[2])
        .text(`${orderData[0].ordertotal}`, 470, table_y[2])

    table_y[table_y.length - 1] = table_y[table_y.length - 1] + 20

    doc
        .lineCap('butt')
        .moveTo(50, table_y[table_y.length - 1])
        .lineTo(560, table_y[table_y.length - 1])
        .stroke()

}

export default invoice