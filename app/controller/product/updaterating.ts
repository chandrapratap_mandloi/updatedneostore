// this api is used for updating rating of product
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const updaterating = (req:Request,res:Response)=>{
    /**
     * @param _id id of product whose rating needs to be updated
     * @param rating new rating given by user
     */
    productmodel.find({"_id":req.body.product_id})
    .then(result=>{
        console.log(req.body)
        console.log(req.body.product_id)
        var update:number;
        var reslt:number;
        let ratingcount:any = result[0].ratingcount + 1;
        update = parseFloat(result[0].rating)+parseFloat(req.body.rating)
        console.log(ratingcount)
        // update = parseFloat(result[0].product_rating) +parseFloat(req.body.rating);
        reslt = update/ratingcount
        
        productmodel.findByIdAndUpdate(req.body.product_id, 
            {$set: {'product_rating':reslt,'ratingcount':ratingcount,'rating':update}})
        .then(result=>{
            res.status(200).json({success:true,status:200,message:'rating updated',product_rating:reslt,ratingcount:ratingcount,product:result})
        })
        .catch(err=>{
            res.status(404).json({success:false,status:404,err:err.details});
        })
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default updaterating