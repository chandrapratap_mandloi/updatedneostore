import cartmodel from '../../models/cartmodel'
import{Request, Response, response} from 'express'

const checkout = (req:Request,res:Response)=>{
    var unique = Date.now()
    var mystr = "ORDER_";
    mystr =  mystr+unique;
    cartmodel.updateMany(
        { $and: [
            { user_id: req.body._id }, {flag:false}
         ]},
        {
          $set: {
            'flag':true,
            'orderno':mystr
          }
        }
    )
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Update Cart",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}   
export default checkout