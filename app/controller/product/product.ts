// api is used for adding new product
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const product = (req:Request,res:Response)=>{


    /**
     * @param product_name
     * @param product_image admin can add upto 12 images of single product
     * @param prod_desc description of product
     * @param product_rating 
     * @param product_producer manufacturer of product
     * @param product_cost
     * @param product_stock 
     * @param product_dimension
     * @param product_material
     * @param category_id id of category to which product belongs
     * @param color_id color id of product
     */
    let images = []
    let files:any=req.files
    for(let i=0; i<req.files.length;i++)
    {
        images.push(files[i].filename);
    }

    let newData = new productmodel({
        product_name:req.body.product_name,
        product_image:images,
        prod_desc:req.body.prod_desc,
        product_rating:req.body.product_rating,
        product_producer:req.body.product_producer,
        product_cost:req.body.product_cost,
        product_stock:req.body.product_stock,
        created_at:req.body.created_at,
        product_dimension:req.body.product_dimension,
        product_material:req.body.product_material,
        category_id:req.body.category_id,
        color_id:req.body.color_id
    })
    
    newData.save()
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Product Added",Product:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default product
