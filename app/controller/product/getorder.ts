//  this api is used for getting order details of user
import cartmodel from '../../models/cartmodel'
import ordermodel from '../../models/ordermodel'
import {Request,Response} from 'express'

const getorder = (req:Request,res:Response)=>{

    /*
        first joined table using lookup 
        it will show details of particular user it takes user id from token
        and group products which has same order number 
    */
   ordermodel.aggregate([
       ([{
            $lookup:
            {
                from: "products",
                localField: "product_id",
                foreignField: "product_id",
                as: "orders"
            }
        },
        // { $match : { customer_id :req.body._id,flag:true } },
        // { $match : { user_id : req.body._id} } ,
        { $match : { user_id : req.body._id,flag:"true"} } ,
        // { $group : { _id : "$item" } },
            { $group: 
                { 
                    _id: "$orderno",
                    products:{$push:"$$ROOT"}
                    // products:{$first:"$$ROOT"}    
                } 
            }
        ]) 
    ])
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Order Details",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default getorder