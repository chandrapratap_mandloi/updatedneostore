//  this api is used for sorting products according to category, color,  price and rating
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const combine = (req:Request,res:Response)=>{

    /**
     * @param category_id to sort products according to category id
     * @param color_id to sort products according to color id
     * @param sortBy to sort product according to rating or price(sortBy=product_rating)
     * @param desc   shows the way of sorting product (sortBy=product_price&desc=false) will sort product according to price in descending order 
     * @param text text to find product
     */
            const a:any ={}
            const {sort='{}', limit, page,skip,  sortBy, text='',   desc,  ...others } = req.query; // destructering
            Object.entries(others).forEach( item => { if(item[1]) a[item[0]]= item[1]; } )
            if(text.trim()){
                console.log(">>>>",text)
                a.product_name={$regex:"^"+text,$options:'i'}
            }
            // console.log(a)
                productmodel.find(a).sort(sortBy ? { [sortBy]: desc == "true" ? -1 : 1 } : {}).limit(parseInt(req.query.limit)).skip(req.query.limit * (req.query.page -1))
                .populate('category_id')
                .populate('color_id')
                // .count()
                .then(result=>{
                    productmodel.find(a).sort(sortBy ? { [sortBy]: desc == "true" ? -1 : 1 } : {})
                    .then(data=>{
                        res.status(200).json({success:true,status:200,product:result,count:data.length})
                    })
                })
                .catch(err=>{
                    res.status(404).json({success:false,status:404,err:err});
                })
        }
export default combine