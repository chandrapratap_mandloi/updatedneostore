import registerUser from '../controller/customers/customer'
import login from '../controller/customers/login'
import editprofile from '../controller/customers/profile'
import upload from '../config/multer'
import verifyToken from '../config/verifytoken'
import getuser from '../controller/customers/getuser'
import addaddress from '../controller/customers/address'
import addaddress1 from '../controller/customers/address1'
import showaddress from '../controller/customers/showaddress'
import getuseraddress from '../controller/customers/getuseraddress'
import editaddress from '../controller/customers/editaddress'
import deleteaddress from '../controller/customers/delteaddress'
import deleteaddress1 from '../controller/customers/deleteaddress1'
import token from '../controller/customers/verifytoken'
import contactus from '../controller/customers/contactus'
import invoice from '../controller/html'
import resetPassword from '../controller/customers/resetPassword'
import SendReport from '../controller/customers/SendReport'
import updateProfile from '../controller/customers/updateProfile'
import inTime from '../controller/customers/inTime'
import getTotalpunch from '../controller/customers/getTotalpunch'
import getOutTime from '../controller/customers/outTime'
import imageInfo from '../config/base64toimg'
import OutTime from '../controller/customers/NewOuttime'
import forgotPassword from '../controller/customers/forgotpassword'
import editaddress1 from '../controller/customers/editaddress1'
export class user{

    public routes(app:any):void{

        app.route('/registerUser')
        .post(registerUser)

        app.route('/intime')
        .post(inTime)
        app.route('/login')
        .post(login)

        app.route('/forgotpassword')
        .post(forgotPassword)
        
        app.route('/contactus')
        .post(contactus)
        
        app.route('/editprofile')
        .post(upload.single('profile_img'),verifyToken,editprofile)

        app.route('/updateprofile')
        .post(verifyToken,updateProfile)
        // .post(imageInfo.single('profile_img'), verifyToken,updateProfile)
        // app.route('/editprofile')
        // .post(verifyToken,editprofile)

        app.route('/getuser')
        .get(verifyToken,getuser)

        app.route('/updatepassword')
        .post(verifyToken,resetPassword)

        app.route('/address')
        .post(verifyToken,addaddress)

        app.route('/address1')
        .post(verifyToken,addaddress1)

        app.route('/showaddress')
        .get(verifyToken,showaddress)

        app.route('/getuseraddress')
        .get(verifyToken,getuseraddress)

        app.route('/editaddress')
        .post(verifyToken,editaddress)
        
        app.route('/editaddress1')
        .post(verifyToken,editaddress1)
        
        app.route('/deleteaddress/:address_id')
        .delete(verifyToken,deleteaddress)

        app.route('/deleteaddress1/:address_id')
        .delete(verifyToken,deleteaddress1)

        app.route('/gettotalpunch/:userid')
        .get(getTotalpunch)

        app.route('/outtime/:userid')
        .get(getOutTime)
        
        app.route('/outTime')
        .post(OutTime)
        
        app.route('/token')
        .get(verifyToken,token)

        app.route('/SendReport')
        .post(SendReport)
        // app.route('/invoice')
        // .get(invoice)
    }
}