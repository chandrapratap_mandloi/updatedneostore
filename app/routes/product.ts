import category from '../controller/product/category'
import upload from '../config/multer'
import product from '../controller/product/product'
import verifyToken from '../config/verifytoken'
import checkout from '../controller/product/checkout'
import findproduct from '../controller/product/findproduct'
import findproducts from '../controller/product/findproducts'
import cart from '../controller/product/cart'
import showcart from '../controller/product/showcart'
import showproducts from '../controller/product/toprated'
import allcategory from '../controller/product/allcategory'
import updaterating from '../controller/product/updaterating'
import getorder from '../controller/product/getorder'
import deletecart from '../controller/product/deletecart'
import suggestion from '../controller/product/suggestion'
import invoice from '../controller/product/invoice'
import cartdata from '../controller/product/updatecart'
import changedata from '../controller/product/changecart'
import downloadinvoice from '../controller/product/invoicedownload'
export class Product{

    public routes(app:any):void{

        app.route('/category')
        .post(upload.single('category_image'),category)

        app.route('/product')
        .post(upload.array('product_image',12),product)

        app.route('/allcategory')
        .get(allcategory)

        app.route('/findproduct')
        .post(findproduct)

        app.route('/findproducts')
        .get(findproducts)

        app.route('/cart')
        .post(verifyToken,cart)

        app.route('/carts')
        .post(verifyToken,changedata)

        app.route('/cartdata')
        .post(verifyToken,cartdata)
        
        app.route('/showcart')
        .get(verifyToken,showcart)

        app.route('/rating')
        .post(verifyToken,updaterating)

        // app.route('/checkout')
        // .get(verifyToken,checkout)

        app.route('/checkout')
        .get(checkout)

        app.route('/showproducts')
        .get(showproducts)

        app.route('/order')
        .get(verifyToken,getorder)


        // app.route('/deletecart/:cart_Id')
        // .delete(verifyToken, deletecart)
        app.route('/deletecart/:cart_id')
        .delete(verifyToken,deletecart)

        app.route('/suggestion')
        .get(suggestion)

        // app.route('/invoice/:_id')
        // .get(invoice)

        app.route('/invoice')
        .post(verifyToken,invoice)

        app.route('/download/:filename')
        .get(downloadinvoice)
        // app.get('/product/report/pdf/:file_name', ProductController.pdfFileDownload);
    }
}