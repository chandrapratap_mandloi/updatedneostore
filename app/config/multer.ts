import multer from 'multer'
import { path } from 'pdfkit';
const storage = multer.diskStorage({
    destination: function (request, file, cb) {
        cb(null, './uploads')
    },
    filename: function (request, file, cb) {
        cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
    
})
let upload = multer({ storage: storage });


// var upload = multer({ //multer settings
//     storage: storage,
//     fileFilter: function (req, file, callback) {
//         var ext = path.extname(file.originalname);
//         if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
//             return callback(new Error('Only images are allowed'))
//         }
//         callback(null, true)
//     },
//     limits:{
//         fileSize: 1024 * 1024
//     }
// }).single('profilepic');
export default upload;