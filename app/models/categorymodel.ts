import mongoose from 'mongoose'


const Schema = mongoose.Schema
 const category = new Schema({
    category_name:{
        type: String,
        unique: true
     },
     category_image:{
        type: String,
        required: true
     }
 })


const categorymodel = mongoose.model('category',category)
export default categorymodel