import mongoose from 'mongoose'

const Schema = mongoose.Schema
 const address = new Schema({
    customer_id:{
        type: String,
        // ref:"registerUser"
    },
    address:{
        type: String,
        required: true
    },
    pincode:{
        type: Number,
        required: true
    },
    city:{
        type: String,
        required: true
    },
    state:{
        type: String,
        required: true
    },
    country:{
        type: String,
        required: true
    },
    flag:{
        type:String,
        default:false
    },
 })

const addressmodel = mongoose.model('address',address)
export default addressmodel