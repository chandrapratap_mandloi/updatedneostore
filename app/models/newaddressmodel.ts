import mongoose from 'mongoose'

const Schema = mongoose.Schema
 const newaddress = new Schema({
    customer_id:{
        type: String,
    },
    address1:[{
        address:{
            type: String,
            required: true
        },
        pincode:{
            type: Number,
            required: true
        },
        city:{
            type: String,
            required: true
        },
        state:{
            type: String,
            required: true
        },
        country:{
            type: String,
            required: true
        },
        flag:{
            type:String,
            default:false
        },
    }],
 })

const newaddressmodel = mongoose.model('newaddress',newaddress)
export default newaddressmodel