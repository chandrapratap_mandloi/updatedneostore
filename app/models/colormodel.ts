import mongoose from 'mongoose'

const Schema = mongoose.Schema
 const color = new Schema({
    color_name:{
        type: String,
        required:true
    },
    color_code:{
        type: String,
        required: true
    },
    color_id:{
        type: String
        // required:true
    }
 })

const colormodel = mongoose.model('color',color)
export default colormodel