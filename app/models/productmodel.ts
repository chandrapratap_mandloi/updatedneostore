import mongoose from 'mongoose'
const Schema = mongoose.Schema
 const product = new Schema({
    category_id:{
        type: String,
        ref:"category"
    },
    color_id:{
        type: String,
        ref:"color"
    },
    product_name:{
        type: String,
        required: true
    },
    product_image:{
        type:[{type:String}],
        required:true
    },
    prod_desc:{
        type: String,
        required: true
    },
    product_rating:{
        type: Number,
        required: true,
        max:5
    },
    product_producer:{
        type: String,
        required: true
    },
    product_cost:{
        type: Number,
        required: true
    },
    product_stock:{
        type: Number,
        required: true
    },
    product_dimension:{
        type: String,
        required: true
    },
    product_material:{
        type: String,
        required: true
    },
    ratingcount:{
        type : Number, 
        default: 1 
    },
    rating:{
        type : Number, 
        default: 0 
    }
 })

const productmodel = mongoose.model('product',product)
export default productmodel