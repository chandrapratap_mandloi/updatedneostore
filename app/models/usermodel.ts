import mongoose from 'mongoose'
var Blob = require('blob');


const Schema = mongoose.Schema

 const user = new Schema({
     
     email:{
        type: String,
        required:true,
        unique: true
     },
     password:{
        type:String,
     },
     gender:{
        type:Number
     },
     phone:{
        type:Number
     },
     first_name:{
        type: String
     },
     last_name:{
        type:String
     },
     dob:{
        type:String
     },
     profile_img:{
        type:String,
     },
     user_img:{
        type:String
     }
 })


 const userModel = mongoose.model('user',user)

export default userModel