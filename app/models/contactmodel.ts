import mongoose from 'mongoose'
const Schema = mongoose.Schema
const contactus = new Schema({
     
    name:{
        type: String,
        required:true
    },
    email:{
        type: String,
        required:true,
        unique: true
    },
    phone:{
        type:Number,
        required:true
    },
    subject:{
        type:String,
        required:true
    }, 
    message:{
        type:String,
        required:true
    }
 })


const contactmodel = mongoose.model('contactus',contactus)

export default contactmodel