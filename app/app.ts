import express from 'express'
import bodyParser from 'body-parser'
import {user} from './routes/customer'
import {Product} from './routes/product'
import {Color} from './routes/color'
import swaggerUI from 'swagger-ui-express'
import swaggerDoc from './swagger.json'
import path from 'path'
import cors from 'cors'
class Application{
public app:express.Application
public routeCustomer:user = new user()
public routeProduct:Product = new Product()
public routeColor:Color=new Color()
    
constructor(){
    this.app=express()
    this.config()
    this.routeCustomer.routes(this.app)
    this.routeProduct.routes(this.app)
    this.routeColor.routes(this.app)
}

config(){
    let publicDir = path.join(__dirname, '../uploads');
    let Dir = path.join(__dirname, '../invoice');
    this.app.use(express.static(publicDir));
    this.app.use(express.static(Dir));
    this.app.use(bodyParser.json())
    this.app.use(cors())
    // this.app.use(bodyParser.urlencoded({extended:false}))
    this.app.use('/swagger',swaggerUI.serve,swaggerUI.setup(swaggerDoc))
    this.app.use(bodyParser.json({limit: '50mb'}));
    this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
}
}
export default new Application().app
